//
//  ViewController.swift
//  Stub to test url session commands
//
//  Created by Steven Hertz on 11/14/21.
//

import UIKit

class ViewController: UIViewController {
    
    
    // used to execute the dataRetrevial from the Web API
    var webApiJsonDecoder = WebApiJsonDecoder()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        /*  --------  Authenticate the teaxher  ------------------------------------    */
        let urlValuesForTeacherAuthenticate = URLValues.urlForTeacherAuthenticate
        webApiJsonDecoder.justAuthenticateProcess(with: urlValuesForTeacherAuthenticate.getUrlRequest(), andSession: urlValuesForTeacherAuthenticate.getSession() ) {(result) in
            
            if case  .failure(.httpError(let respne)) = result, respne.statusCode == 401 {
                print("error authi")
            }
            
           
            // *** No errors do the process and get the returned object ***
            
            // get the data from result
            guard let aAuthenticateReturnData = try? result.get() else {fatalError("no data") }
            
            // decode the authenticate data
            let returnFromDecodingAuthentication: Result<AuthenticateReturnObjct,GetResultOfNetworkCallError> = self.webApiJsonDecoder.processTheData(with: aAuthenticateReturnData)

            // verify and get the response object
            guard let aAuthenticateReturnObjct = try? returnFromDecodingAuthentication.get() else {
                if case Result<AuthenticateReturnObjct,GetResultOfNetworkCallError>.failure(let getResultOfNetworkCallError) = returnFromDecodingAuthentication {
                    self.webApiJsonDecoder.processTheError(with: getResultOfNetworkCallError)
                }
                return
            }
            
            
            //  *** OK we got the returned object - Use it  ***
                    
            self.webApiJsonDecoder.theAuthenticateReturnObjct = aAuthenticateReturnObjct
            guard let userId = self.webApiJsonDecoder.theAuthenticateReturnObjct?.authenticatedAs.id else {fatalError("id error")}


            
            
            /*  --------  Get teacher User info  ------------------------------------    */
             let urlValuesForUserInfo = URLValues.urlForUserInfo(userID: String(userId))
             self.webApiJsonDecoder.sendURLReqToProcess(with: urlValuesForUserInfo.getUrlRequest(), andSession: urlValuesForUserInfo.getSession() ) {(data) in
                 
                 print(String(data: data, encoding: .utf8) as Any)
                 // OK we are fine, we got data - so lets write it to a file so we can retieve it
                 
                 
                 // decode the json data
                 
                 let returnFromUserInfo: Result<UserInfoReturnObjct,GetResultOfNetworkCallError> = self.webApiJsonDecoder.processTheData(with: data)
                 
                 guard let aUserInfoReturnObjct = try? returnFromUserInfo.get() else {
                     if case Result<UserInfoReturnObjct,GetResultOfNetworkCallError>.failure(let getResultOfNetworkCallError) = returnFromUserInfo {
                         self.webApiJsonDecoder.processTheError(with: getResultOfNetworkCallError)
                     }
                     return
                 }
                 self.webApiJsonDecoder.theUserInfoReturnObjct = aUserInfoReturnObjct
                
                
                                
             }

        }
        print("from view did load")
    }
    
    
}

