//
//  LessonKeyReturnObjct.swift
//  Stub to test url session commands
//
//  Created by Steven Hertz on 11/15/21.
//

import Foundation

struct LessonKeyReturnObjct: Codable {
    let lessonId: String
    let lessonType: String
    let lessonTeacherId: String
    let lessonName: String
    let lessonDescription: String

    func getCodeKey() -> String {
        guard let apiKey = self.lessonDescription.slice(from: "appi4<", to: ">appi4"), !apiKey.isEmpty else {fatalError("can not get api key")}
        return apiKey
    }

}

